package ca.sb.finalproject.model;

import java.util.List;

import org.json.JSONObject;

import com.activeandroid.query.*;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import ca.sb.finalproject.MainActivity;
import ca.sb.finalproject.R;
import ca.sb.finalproject.StatsActivity;

public class StatListAdapter extends ArrayAdapter<Stats> 
{
	private List<Stats> list;
	private final Activity context;
	// -------------------------------
	public StatListAdapter(Activity context, List<Stats> list) 
	{
		super(context, R.layout.player_list_element, list);
		this.context = context;
		this.list = list;
	}
	public void setList(List<Stats> l)
	{
		list = l;
	}
	@Override
	public int getCount()
	{
		return list.size();
	}
	@Override
	public Stats getItem(int position)
	{
		return list.get(position);
	}
	@Override
	public long getItemId(int position)
	{
		return position;
	}
	static class ViewHolder
	{
		public TextView textViewPlayerName;
		public Button buttonPlay;
		public Button buttonStats;
	}
	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final ViewHolder holder;

		if (convertView == null)
		{
			LayoutInflater inflator = context.getLayoutInflater();
			convertView = inflator.inflate(R.layout.player_list_element,
					null);
			holder = new ViewHolder();

			holder.textViewPlayerName = (TextView) convertView
			    .findViewById(R.id.textView1);
			holder.buttonPlay = (Button) convertView
			    .findViewById(R.id.button1);
			holder.buttonStats = (Button) convertView
			    .findViewById(R.id.button2);

			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}

		final Stats stat = list.get(position);
		holder.buttonPlay.setTag(stat);
		holder.buttonStats.setTag(stat);
		holder.textViewPlayerName.setText(stat.playerName);

		holder.buttonPlay.setOnClickListener(new View.OnClickListener() 
		{
			@Override
			public void onClick(View v)
			{
				Stats stat = (Stats) v.getTag();
				// Launching new Activity on selecting single List Item
				Intent intent = new Intent(context,
						MainActivity.class);
				// sending data to new activity
				Bundle bundle = new Bundle();
				long id = stat.getId();
				bundle.putLong("id", stat.getId());
				intent.putExtras(bundle);
				context.startActivity(intent);
			}
		});

		holder.buttonStats.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Stats stat= (Stats) v.getTag();
				// Launching new Activity on selecting single List Item
				Intent intent = new Intent(context, StatsActivity.class);
				// sending data to new activity
				Bundle bundle = new Bundle();
				bundle.putLong("id", stat.getId());
				intent.putExtras(bundle);
				context.startActivity(intent);
			}
		});
		return convertView;
	}
}