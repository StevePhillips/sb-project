package ca.sb.finalproject.model;
// -------------------------------------
import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
// ------------------------------
@Table(name="Stats")
public class Stats extends Model
{
  // ----------------------
  // Player name
  // -------------
  @Column(name="playername")
  public String playerName;
  // ----------------------
  // All time bombs laid
  // ---------------------
  @Column(name="bombslaid")
  public int bombslaid;
  // --------------------
  // Total deaths
  // -----------------
  @Column(name="deaths")
  public int deaths;
  // -------------------------
  // Blocks destroyed
  // --------------------
  @Column(name="blocksDestroyed")
  public int blocksDestroyed;
  // -------------------------
  public Stats()
  {
    super();
  }
}
