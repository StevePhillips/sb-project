package ca.sb.finalproject.animating;

import ca.sb.finalproject.MainActivity;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import ca.sb.finalproject.R;

public class Explosion extends AnimatedSprite
{
  public Explosion(Point startPos, Point dimen)
  {
    super(
      BitmapFactory.decodeResource(
        MainActivity.activity.getResources(),
        R.drawable.explosion),
      startPos.x, startPos.y,
      dimen.x, dimen.y,
      6, 6);
  }
  @Override
  public void update(long gameTime)
  {
    super.update(gameTime);
    if(getCurrentFrame() == 5)
    {
      die();
    }
  }
}
