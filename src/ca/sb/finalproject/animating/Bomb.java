package ca.sb.finalproject.animating;

import android.graphics.BitmapFactory;
import android.graphics.Point;
import ca.sb.finalproject.MainActivity;
import ca.sb.finalproject.R;

public class Bomb extends AnimatedSprite
{
  public Bomb(Point loc)
  {
    super(
      BitmapFactory.decodeResource(
        MainActivity.activity.getResources(),
        R.drawable.bomb),
      loc.x, loc.y,
      32, 32,
      1, 6);
  }
  @Override
  public void update(long gameTime)
  {
    if(getCurrentFrame() == 5)
    {
      explode();
    }
    super.update(gameTime);
  }
  private void explode()
  {
    GamePanel.entities.add(new Explosion(new Point(getX(),getY()),
                                         new Point(32, 32)));
    int width = getSpriteWidth(), height = getSpriteHeight();
    for(BreakableBlock block : GamePanel.breakable)
    {
      int bwidthp = block.getX()+block.getSpriteWidth(),
          bheightp= block.getY()+block.getSpriteHeight();
      // is the position of block greater than or equal to the bombs?
      if(getX() <= block.getX() && block.getX() <= getX()+getSpriteWidth()&&//getY() <= block.getY() &&
      // and less than the bombs width and height
         getY() <= block.getY() && block.getY() <= getY()+getSpriteHeight() ||
      // or the bottom point of the block is in range of the bombs explosion
         getX() <= bwidthp && bwidthp <= getX()+getSpriteWidth()&&
         getY() <= bheightp&& bheightp<= getY()+getSpriteHeight() ||
         // ------------------------------------------------------
         getX() <= bwidthp && bwidthp <= getX()+getSpriteWidth()&&
         getY() <= block.getY()&&block.getY()<=getY()+getSpriteHeight()||
         // ----------------------------------------------------
         getX() <= block.getX() && block.getX() <= getX()+getSpriteWidth()&&
         getY() <= bheightp && bheightp <= getY()+getSpriteHeight()) 
        block.crumble();
    }
    die();
  }
}
