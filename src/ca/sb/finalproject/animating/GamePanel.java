package ca.sb.finalproject.animating;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ca.sb.finalproject.MainActivity;
import ca.sb.finalproject.R;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.support.v4.view.VelocityTrackerCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnTouchListener;

public class GamePanel
extends SurfaceView
implements SurfaceHolder.Callback, OnTouchListener
{
  private static final String tag = GamePanel.class.getSimpleName();
  public static MainThread thread;
  public static List<AnimatedSprite> entities = new ArrayList<AnimatedSprite>();
  public static List<BreakableBlock> breakable= new ArrayList<BreakableBlock>();
  // ----------------------------
  private String avgFps; // fps to be displayed at
  public void setAvgFps(String avgFps)
  {
    this.avgFps = avgFps;
  }
  public boolean onTouch(View v, MotionEvent e)
  {
    return true;
  }
  // ------------------------------
  public GamePanel(Context context)
  {
    super(context);
    // --------------------------
    //entities = new ArrayList<AnimatedSprite>();
    // -------------------------------
    getHolder().addCallback(this);
    
    entities.add(new Player(new Point(200, 200)));
    for(int i = 0; i < 20; i++)
    {
      entities.add(new Bomb(
           new Point(i*10+10, i*10+10)));
    }
    for(int i = 0; i < 20; i++)
    {
      breakable.add(new BreakableBlock(R.drawable.breakablewall1,i*32,200));
    }
    thread = new MainThread(getHolder(), this);
    setFocusable(true);
  }
  // ------------------------------------------
  // Track motion of users finger to determine
  // direction players character should move
  // ----------------------------------------
  private VelocityTracker velTracker = null;
  @Override
  public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
  {
  }
  @Override
  public void surfaceCreated(SurfaceHolder holder)
  {
    thread.setRunning(true);
    thread.start();
  }
  @Override
  public void surfaceDestroyed(SurfaceHolder holder)
  {
    Log.d(tag, "Surface is destroyed");
    // -------------------------------
    boolean retry = true;
    while(retry)
    {
      try
      {
        thread.join();
        retry = false;
      }
      catch(InterruptedException e)
      {
      }
    }
    Log.d(tag, "Thread was shut down cleanly");
  }
  int movePointer = 0;
  @Override
  public boolean onTouchEvent(MotionEvent event)
  {
    int index = event.getActionIndex();
    int action = event.getActionMasked();
    int pointerId = event.getPointerId(index);
    // --------------------------------------
    switch(action)
    {
    case MotionEvent.ACTION_DOWN:
      Log.d("", "pointer: "+pointerId);
      if(event.getX() > this.getWidth()/2)
        movePointer = pointerId;
      if(velTracker == null)
        velTracker = VelocityTracker.obtain();
      else
        velTracker.clear();
      // ---------------------
      velTracker.addMovement(event);
      // -----------------------------------
      break;
      // -----------------------
    case MotionEvent.ACTION_MOVE:
      velTracker.addMovement(event);
      // ------------------------
      velTracker.computeCurrentVelocity(1000);
      // ----------------------------------
      Player player = (Player)entities.get(0);
      // --------------------------------------
      float velocityx = velTracker.getXVelocity(pointerId);
      float velocityy = velTracker.getYVelocity(pointerId);
      if(velocityx > 800.0)
      {
        player.setVelocity(5.0f, 0);
        player.setSpriteDir(2);
      }
      else if(velocityx < -800.0)
      {
        player.setVelocity(-5.0f, 0);
        player.setSpriteDir(1);
      }
      if(velocityy > 800.0)
      {
        player.setVelocity(0, 5.0f);
        player.setSpriteDir(0);
      }
      else if(velocityy < -800.0)
      {
        player.setVelocity(0, -5.0f);
        player.setSpriteDir(3);
      }
      break;
    case MotionEvent.ACTION_UP:
      if(pointerId == movePointer)
        ((Player)entities.get(0)).setVelocity(0.0f, 0.0f);
      break;
    case MotionEvent.ACTION_CANCEL:
      velTracker.recycle();
      break;
      // -------------------------------------------
      // This case is for a second finger to use to
      // drop bombs
      // -------------
    case MotionEvent.ACTION_POINTER_DOWN:
      Player plr = (Player) entities.get(0);
      entities.add(new Bomb(new Point(plr.getX(), plr.getY())));
      MainActivity.currentPlayer.bombslaid++;
      break;
    }
    return true;
  }
  public void render(Canvas canvas)
  {
    canvas.drawColor(Color.BLACK);
    for(int i = 0; i < entities.size(); i++)
      entities.get(i).draw(canvas);
    for(BreakableBlock block : breakable)
      block.draw(canvas);
    // ----------------
    displayFps(canvas, avgFps);
  }
  public void update()
  {
    //bomb.update(System.currentTimeMillis());
    for(int i = 0; i < entities.size(); i++)
    {
      entities.get(i).update(System.currentTimeMillis());
      if(!entities.get(i).living())
        entities.remove(i);
    }
    for(int i = 0; i < breakable.size(); i++)
    {
      breakable.get(i).update(System.currentTimeMillis());
      if(!breakable.get(i).living())
        breakable.remove(i);
    }
  }
  private void displayFps(Canvas canvas, String fps)
  {
    if(canvas != null && fps != null)
    {
      Paint paint = new Paint();
      paint.setARGB(255, 255, 255, 255);
      canvas.drawText(fps, this.getWidth()-50, 20, paint);
    }
  }
}
