package ca.sb.finalproject.animating;
// --------------------------
import android.graphics.BitmapFactory;
import android.graphics.Point;
import ca.sb.finalproject.MainActivity;
// --------------------------------
import ca.sb.finalproject.R;
// -------------------------
public class Player extends AnimatedSprite
{
  private float velX, velY;
  // -----------------------
  public Player(Point startPos)
  {
    super(BitmapFactory.decodeResource(
            MainActivity.activity.getResources(), R.drawable.pirategirl),
          startPos.x, startPos.y,
          32, 48,
          4, 4);
  }
  public void setVelocity(float valX, float valY)
  {
    velX=valX;velY=valY;
  }
  public void update(long gameTime)
  {
    if(velX == 0 && velY == 0)
      return;
    x+=(int)velX;y+=(int)velY;
    super.update(gameTime);
  }
  public void setSpriteDir(int index)
  {
    sourceRect.top = index*48;
    sourceRect.bottom = sourceRect.top+getSpriteHeight();
  }
}
